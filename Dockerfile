# FROM node:latest
FROM keymetrics/pm2:latest-alpine
LABEL maintainer "kamael@hotmail.com.tw"

WORKDIR /data

# Install git, nodejs
RUN apk update && apk add git

# Install eth-netstats
RUN git clone https://github.com/cubedro/eth-netstats
WORKDIR /data/eth-netstats
COPY app.json .

RUN npm install
RUN npm install -g grunt-cli
RUN grunt

ENV WS_SECRET=""
EXPOSE 3000
CMD ["npm", "start"]
